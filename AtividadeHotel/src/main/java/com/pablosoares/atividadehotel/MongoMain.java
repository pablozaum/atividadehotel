/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablosoares.atividadehotel;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
/**
 *
 * @author Pablozaum
 */
public class MongoMain {

    public MongoDatabase conectarMongo(){
        ConnectionString connString = new ConnectionString(
                "mongodb+srv://admin:admin@atividade-hotel-tlwbl.gcp.mongodb.net/test?retryWrites=true&w=majority"
        );
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connString)
                .retryWrites(true)
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        
        MongoDatabase database = mongoClient.getDatabase("AtividadeHotel");
//        MongoCollection clientes = database.getCollection("Clientes");
//        MongoCollection quartos = database.getCollection("Quartos");
//        MongoCollection reserva = database.getCollection("Reserva");
        
        return database;
    }
    
//    public int menuPrincipal(){
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("***********HotelZaum***********");
//        System.out.println("Selecione a opção desejada:");
//        System.out.println("1 - Cadastrar Clientes.");
//        System.out.println("2 - Consultar Clientes.");
//        System.out.println("3 - Realizar Reserva.");
//        System.out.println("4 - Consultar Reserva.");
//        System.out.println("5 - Encerrar Programa.");
//        int opcao = scanner.nextInt();
//        
//        return opcao;
//    }
    
//    public static void main(String[] args) {

        
//        MongoDAO dao = new MongoDAO();
//        
//        Scanner scanner = new Scanner(System.in);
//        MongoMain menu = new MongoMain();
//        MongoDatabase data = menu.conectarMongo();
//        System.out.println(""+menu.menuPrincipal()+"");
//        int opcao = menu.menuPrincipal();
//        
//        
//        switch(opcao){
//            case 1:
//                System.out.println("Digite o nome do Cliente: ");
//                String nome = scanner.next();
//                System.out.println("Digite a Idade: ");
//                int idade = scanner.nextInt();
//                System.out.println("Digite o email: ");
//                String email = scanner.next();
//                System.out.println("****************************");
//                System.out.println("Confirmar o cadastro? ( S / N )");
//                System.out.println(""+nome+" \n"+idade+" \n"+email+"");
//                String conf = scanner.next();
//                if(conf.equalsIgnoreCase("s")){
//                    MongoCollection clientes = data.getCollection("Clientes");
//                    clientes.insertOne(dao.addClientes(nome, idade, email));
//                    }
//                    else{
//                    break;
//                }
//        }
//        
        
        
         
        
    }
//}
