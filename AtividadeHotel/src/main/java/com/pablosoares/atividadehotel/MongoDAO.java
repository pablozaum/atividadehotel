/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pablosoares.atividadehotel;

import java.io.Serializable;
import org.bson.Document;

/**
 *
 * @author Pablozaum
 */
public class MongoDAO {
    
    
    public Document addClientes(String nome, int idade, String email){
    
        Document document = new Document("Nome", ""+nome+"");
        document.append("Idade", idade);
        document.append("Email", ""+email+"");
        
        return document;
    }
    
    public Document addReserva(Document cliente, Document quarto){
        
        Document document = new Document("Cliente", ""+cliente+"");
        document.append("Quarto", quarto);
        
        return document;
    }
    
    public Document addQuartos(int numero, String tipo){
        
        Document document = new Document("Numero", ""+numero+"");
        document.append("Tipo", ""+tipo+"");
        
        return document;
    }
}
